variable "domain_name" {
    type = string
    description = "Domain name for the website"
}

variable "sub_domain_name" {
    type = string
    description = "Sub-domain name for the website"
}